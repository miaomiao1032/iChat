package org.duomn.ichat.net;

import org.duomn.ichat.entity.Message;
import org.duomn.ichat.entity.MsgCatalog;
import org.duomn.ichat.entity.MsgType;
import org.duomn.ichat.exception.MsgLoseException;
import org.duomn.ichat.net.impl.ChatMsgSender;
import org.duomn.ichat.thread.TCPReceiverThread;
import org.duomn.ichat.thread.TCPThreadPORT;
import org.duomn.ichat.util.Const;
import org.junit.Test;

public class ChatMsgHandleTest {

	@Test
	public void testTransfer() {
		Const.put(Const.USER_SELF_ID, 0);
		TCPReceiverThread server = new TCPReceiverThread(8090); // 服务端线程启动
		server.start();
		
//		for (int i = 0; i < 10; i++) {
			TCPThreadPORT port = new TCPThreadPORT("127.0.0.1", 8090); // 模拟的一个好友
			port.setMsgSender(new ChatMsgSender());
			port.setUIHandler(new UIChatMsgHandler() {
				public void handle(Message msg) {
					System.out.println("-------------------我需要把这条消息显示到我们的聊天窗口。" + msg.data);
				}
			});
			
			if (port.openSession()) {
				try {
					Message msg = new Message();
					msg.msgCatalog = MsgCatalog.CHAT_MSG;
					msg.msgType = MsgType.CHAT_INFO;
					msg.data = "我是客户端";
					port.send(msg);
				} catch (MsgLoseException e) {
					e.printStackTrace();
				}
				port.closeSession();
			}
//		}
		
		server.close();
	}

}
