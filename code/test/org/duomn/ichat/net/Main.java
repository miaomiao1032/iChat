package org.duomn.ichat.net;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

public class Main {
  public static void main(String[] args) throws Exception {
    JFrame f = new JFrame("Styles Example 2");
    StyleContext sc = new StyleContext();
    final DefaultStyledDocument doc = new DefaultStyledDocument(sc);
    JTextPane pane = new JTextPane(doc);

    Style defaultStyle = sc.getStyle(StyleContext.DEFAULT_STYLE);
    final Style mainStyle = sc.addStyle("MainStyle", defaultStyle);
    StyleConstants.setLeftIndent(mainStyle, 16);
    StyleConstants.setRightIndent(mainStyle, 16);
    StyleConstants.setFirstLineIndent(mainStyle, 16);
    StyleConstants.setFontFamily(mainStyle, "serif");
    StyleConstants.setFontSize(mainStyle, 12);

    final Style cwStyle = sc.addStyle("ConstantWidth", null);
    StyleConstants.setFontFamily(cwStyle, "monospaced");
    StyleConstants.setForeground(cwStyle, Color.green);

    final Style heading2Style = sc.addStyle("Heading2", null);
    StyleConstants.setForeground(heading2Style, Color.red);
    StyleConstants.setFontSize(heading2Style, 16);
    StyleConstants.setFontFamily(heading2Style, "serif");
    StyleConstants.setBold(heading2Style, true);
    StyleConstants.setLeftIndent(heading2Style, 8);
    StyleConstants.setFirstLineIndent(heading2Style, 0);

    doc.setLogicalStyle(0, mainStyle);
    doc.insertString(0, "asdfasdfasdfasdfasdfasdfasdf", null);

    doc.setCharacterAttributes(1, 2, cwStyle, false);
    doc.setCharacterAttributes(3, 1, cwStyle, false);
    doc.setCharacterAttributes(4, 1, cwStyle, false);
    doc.setCharacterAttributes(5, 1, cwStyle, false);
    doc.setCharacterAttributes(6, 1, cwStyle, false);
    doc.setCharacterAttributes(7, 1, cwStyle, false);
    doc.setCharacterAttributes(8, 1, cwStyle, false);
    doc.setCharacterAttributes(9, 1, cwStyle, false);
    doc.setCharacterAttributes(10, 1, cwStyle, false);
    doc.setCharacterAttributes(11, 1, cwStyle, false);

    doc.setParagraphAttributes(0, 1, heading2Style, false);

    f.add(new JScrollPane(pane));
    f.setSize(400, 300);
    f.setVisible(true);
  }
}