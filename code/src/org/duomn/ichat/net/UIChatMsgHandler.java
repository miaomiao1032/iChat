package org.duomn.ichat.net;

import org.duomn.ichat.entity.Message;


public interface UIChatMsgHandler {

	void handle(Message msg);
	
}
