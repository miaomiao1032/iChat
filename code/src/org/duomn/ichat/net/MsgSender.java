package org.duomn.ichat.net;

import java.io.ObjectOutputStream;

import org.duomn.ichat.exception.MsgLoseException;

/**
 * 网络发送的接口，主动线程和被动线程均通过该接口发送数
 * @author duomn
 *
 */
public interface MsgSender {
	
	void injectSendStream(ObjectOutputStream out);
	
	void send(Object obj) throws MsgLoseException;
	
	void sendClose() throws MsgLoseException;
	
}
