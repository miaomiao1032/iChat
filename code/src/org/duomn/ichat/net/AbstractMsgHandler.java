package org.duomn.ichat.net;

import org.duomn.ichat.entity.Message;
import org.duomn.ichat.exception.MsgHandleException;
import org.duomn.ichat.thread.TCPThreadAbstract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 简化没有初始化对象都需要处理一个对象的问题
 * @author duomn
 *
 */
public abstract class AbstractMsgHandler implements MsgHandler {
	private static Logger logger = LoggerFactory.getLogger(AbstractMsgHandler.class);
	
	protected TCPThreadAbstract chatThread;
	
	public AbstractMsgHandler(TCPThreadAbstract chatThread, Message message) {
		this.chatThread = chatThread;
		try {
			handMsg(message);
		} catch (MsgHandleException e) {
			// TODO 处理该异常
			logger.error("Init AbstractMsgHandler with a Message happend Exception", e);
		}
	}
	
	protected abstract void handMsg(Message msg) throws MsgHandleException;
}
