package org.duomn.ichat.net;

import java.io.IOException;
import java.net.SocketException;

/**
 * 消息的处理接口：<br>
 * <p>主要解决不同传输类型需要使用不同的Socket连接，但是需要保证<br>
 * 使用唯一的一个端口的难题，实现不同对象类型的解耦</p>
 * 直接处理流中的内容，让聊天，文件传输，以及其他类型的传输对象，<br>
 * 均能通过流的方式进行传输，在接收消息的监听线程中，使用委托对象，<br>
 * 用不同的Handler对不同的消息进行处理。
 * @author duomn
 *
 */
public interface MsgHandler {
	
	/** 除了第一次接受到的消息外，每次接受消息都会调用该方法 */
	public void handle() throws ClassNotFoundException, SocketException, IOException;

}
