package org.duomn.ichat.net;

import java.io.ObjectOutputStream;

/**
 * 把注入Socket的发送流的逻辑提取到抽象类中
 * @author duomn
 *
 */
public abstract class AbstractMsgSender implements MsgSender {

	protected ObjectOutputStream out;

	@Override
	public void injectSendStream(ObjectOutputStream out) {
		this.out = out;
	}
	
}
