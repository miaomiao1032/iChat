package org.duomn.ichat.net;

import java.io.File;

/**
 * 文件处理的界面处理接口，对于主动方，和接受者方的处理方式不同 
 * @author duomn
 *
 */
public interface UIFileMsgHandler {

	
	/** 同意文件传输时，获取文件信息 */
	File accessFile(String fileId, boolean sponsor);
	
	/** 拒绝文件传输时，处理方法 */
	void refuseFile(String fileId, boolean sponsor);
	
	/**
	 * 处理文件传输的进度
	 * @param progressSize
	 * @param totalSize
	 */
	void doProgress(String fileId, long progressSize, long totalSize, boolean sponsor);
	

}
