package org.duomn.ichat.net.impl;

import java.io.IOException;

import org.duomn.ichat.entity.Message;
import org.duomn.ichat.entity.MsgType;
import org.duomn.ichat.exception.MsgLoseException;
import org.duomn.ichat.net.AbstractMsgSender;
import org.duomn.ichat.util.Const;

public class ChatMsgSender extends AbstractMsgSender {
	
	@Override
	public void send(Object message) throws MsgLoseException {
		try {
			out.writeObject(message);
		} catch (IOException e) {
			throw new MsgLoseException("发送聊天消息失败", (Message) message);
		}
	}
	
	@Override
	public void sendClose() throws MsgLoseException {
		Message message = new Message();
		message.msgType = MsgType.CLOSE_SESSION;
		send(message);
	}
	
	public void sendLogin() throws MsgLoseException {
		Message message = new Message();
		message.src = Const.get(Const.USER_SELF_ID);
		message.data = Const.get(Const.USER_SELF);
		send(message);
	}
	
	public void sendLoginOut() throws MsgLoseException {
		Message message = new Message();
		message.src = Const.get(Const.USER_SELF_ID);
		send(message);
	}

}
