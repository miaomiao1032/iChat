package org.duomn.ichat.net.impl;

public enum FileMsgType {
	
	/** 第一次传输时的文件信息 */
	FILE_INFO,
	
	/** 传输文件时的，头信息 */
	FILE_TRAN_HEAD,

	/** 同意接受文件 */
	FILE_ACCESS_REV,
	
	/** 拒绝接受文件 */
	FILE_REFUSE_REV,
	
}
