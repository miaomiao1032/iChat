package org.duomn.ichat.dao;

import java.util.Map;

import org.duomn.ichat.entity.User;

public interface IUserDao {

	Map<Integer, User> listFriends();
	
}
