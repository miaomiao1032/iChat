package org.duomn.ichat.dao;

import java.util.HashMap;
import java.util.Map;

import org.duomn.ichat.entity.User;

public class UserDaoForTest implements IUserDao {
	String[] names = {"张三", "李四", "王五"};
	
	Map<Integer, User> users = new HashMap<Integer, User>();
	{
		for (int i = 0; i < 3; i++) {
			User user = new User();
			user.setHost("127.0.0.1");
			user.setPort(8090 + i);
			user.setId(i);
			user.setName(names[i]);
			user.setStatus("天气不错啊");
			users.put(i, user);
		}
	}
	
	@Override
	public Map<Integer, User> listFriends() {
		return users;
	}
	
	
	public void updateUserStatus(User user) {
		
	}
	
}
