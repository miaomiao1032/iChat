package org.duomn.ichat.exception;

import org.duomn.ichat.entity.Message;

/**
 * 消息丢失异常
 * @author duomn
 *
 */
public class MsgLoseException extends Exception {

	private static final long serialVersionUID = 7574066217834003531L;
	
	private Message msg;
	
	public MsgLoseException(String errMsg, Message message) {
		super(errMsg);
		this.msg = message;
	}

	/** 获取发送失败的消息 */
	public Message getMsg() {
		return msg;
	}
}
