package org.duomn.ichat.exception;

public class MsgHandleException extends RuntimeException {
	
	private static final long serialVersionUID = -5188360847982143086L;
	
	public MsgHandleException() {
	}

	public MsgHandleException(String msg) {
		super(msg);
	}
	
	public MsgHandleException(Throwable t) {
		super(t);
	}
	
	public MsgHandleException(String msg, Throwable t) {
		super(msg, t);
	}
}
