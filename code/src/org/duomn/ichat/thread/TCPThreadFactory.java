package org.duomn.ichat.thread;

import org.duomn.ichat.net.UIChatMsgHandler;
import org.duomn.ichat.net.impl.ChatMsgSender;


public class TCPThreadFactory {

	public static TCPThreadAbstract createChatMsgPortThread(String host, int port) { 
		return createChatMsgPortThread(host, port, null);
	}
	
	public static TCPThreadAbstract createChatMsgPortThread(String host, int port, UIChatMsgHandler uiHandler) {
		TCPThreadAbstract chatThread = new TCPThreadPORT(host, port);
		chatThread.setMsgSender(new ChatMsgSender());
		chatThread.setUIHandler(uiHandler);
		return chatThread;
	}
	
}
