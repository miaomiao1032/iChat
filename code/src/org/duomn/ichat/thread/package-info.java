/**
 * 为GUI层提供线程服务的工具类，把GUI中的复杂操作隔离出来
 */
/**
 * @author duomn
 *
 */
package org.duomn.ichat.thread;