package org.duomn.ichat.thread;

import java.net.Socket;


/**
 * 聊天被动(PASV)线程，由监听线程（Receiver）创建
 * 在主动线程终止后自行终止
 * @author duomn
 *
 */
public class TCPThreadPASV extends TCPThreadAbstract {
	
	TCPThreadPASV(Socket socket) {
		this.socket = socket;
	}
	
	public boolean openSession() {
		return initStream();
	}

}
