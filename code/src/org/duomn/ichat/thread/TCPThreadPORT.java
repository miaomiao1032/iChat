package org.duomn.ichat.thread;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 聊天的主动(PORT)线程
 * 在聊天结束后，需要发送关闭消息，用来关闭被动线程，自己关闭socke并退出
 * @author duomn
 *
 */
public class TCPThreadPORT extends TCPThreadAbstract {
	
	private static Logger logger = LoggerFactory.getLogger(TCPThreadPORT.class);
	
	private String host;
	private int port;
	
	public TCPThreadPORT(String host, int port) { // 不再构造方法中处理复杂的逻辑
		this.host = host;
		this.port = port;
	}
	
	public boolean openSession() {
		try {
			socket = new Socket(host, port);
		} catch (UnknownHostException e) {
			logger.warn("主机[" + host + "]无法连通");			
			return false;
		} catch (SocketException e) {
			logger.warn("端口[" + port + "]无法建立监听");
			return false;
		} catch (IOException e) {
			logger.error("建立会话发生异常", e);
			return false;
		}
		
		return initStream();
	}

}
