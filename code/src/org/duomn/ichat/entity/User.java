package org.duomn.ichat.entity;

import java.io.Serializable;

public class User implements Serializable {
	private static final long serialVersionUID = 6874626819942584459L;

	private int id;
	
	private String host;
	
	private int port;
	
	private String name;
	
	private String status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String toString() {
		return "id:" + id + ", name=" + name + ", status=" + status;
	}
}
