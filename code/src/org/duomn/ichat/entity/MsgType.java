package org.duomn.ichat.entity;

import java.io.Serializable;

public enum MsgType implements Serializable {
	
	/** 关闭SESSION的消息 */
	CLOSE_SESSION,
	
	/** 上线消息 */
	LOGIN_IN,
	
	/** 下线消息 */
	LOGIN_OUT,
	
	/** 聊天消息 */
	CHAT_INFO,
	
	/** 特殊消息的头信息 */
	ESP_HEAD,
	
}
