package org.duomn.ichat.entity;

import java.io.Serializable;

/**
 * 基本的message，只包含消息的来源
 * 该类用于网络传输，为减少序列化的大小，不设置访问属性的方法
 * @author duomn
 *
 */
public class Message implements Serializable {
	private static final long serialVersionUID = 6784672228456435462L;
	/** 消息来源的id */
	public int src;
	
	/** 消息的大类比，默认值消息 */
	public MsgCatalog msgCatalog = MsgCatalog.CHAT_MSG;
	
	/** 消息的类型 */
	public MsgType msgType;
	
	/** 消息的数据部分 */
	public Object data;
}
