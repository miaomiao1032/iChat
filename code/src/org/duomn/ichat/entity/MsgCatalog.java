package org.duomn.ichat.entity;

/**
 * 代表消息的大类，不使用同一个socket进行通信的大类别
 * @author duomn
 *
 */
public enum MsgCatalog {

	/** 聊天消息 */
	CHAT_MSG,
	
	/** 文件传输 */
	FILE_TRAN,
}
