package org.duomn.ichat.entity;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FontAttribute implements Serializable, Cloneable {

	private static final long serialVersionUID = -8510483928028197183L;
	private static Logger logger = LoggerFactory.getLogger(FontAttribute.class);
	
	private String fontName;
	private int fontSize;
	private boolean bold;
	private boolean italic;
	private boolean underline; 

	public FontAttribute() {
		
	}

	public String getFontName() {
		return fontName;
	}

	public void setFontName(String fontName) {
		this.fontName = fontName;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

	public boolean isBold() {
		return bold;
	}

	public void setBold(boolean bold) {
		this.bold = bold;
	}

	public boolean isItalic() {
		return italic;
	}

	public void setItalic(boolean italic) {
		this.italic = italic;
	}

	public boolean isUnderline() {
		return underline;
	}

	public void setUnderline(boolean underline) {
		this.underline = underline;
	}
	
	public FontAttribute clone() {
		FontAttribute fa = null;
		try {
			fa = (FontAttribute) super.clone();
		} catch (CloneNotSupportedException e) {
			fa = new FontAttribute();
			logger.warn("clone exception, create a new FontAttribute");
		}
		return fa;
	}
	
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append(this.fontName).append("#")
		.append(this.fontSize).append("#")
		.append(this.bold).append("#")
		.append(this.italic).append("#")
		.append(this.underline);
		return buf.toString();
	}
}
