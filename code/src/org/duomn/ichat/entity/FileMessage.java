package org.duomn.ichat.entity;

import org.duomn.ichat.net.impl.FileMsgType;

/**
 * 文件类型的消息
 * @author duomn
 *
 */
public class FileMessage extends Message {
	private static final long serialVersionUID = 3909376663607518723L;
	
	public String storageFile;
	
	public long fileSize;
	
	public FileMsgType fileMsgType;

}
