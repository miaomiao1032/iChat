package org.duomn.ichat.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	private static final String DEFAULT_DATE_FORMAT = "HH:mm:ss";
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
	
	public synchronized static String getNowStr() {
		return sdf.format(new Date());
	}
}
