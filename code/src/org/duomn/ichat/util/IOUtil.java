package org.duomn.ichat.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IOUtil {
	
	public static void copyStream(InputStream in, OutputStream out) throws IOException {
		int len = 0;
		int bufLen = 8 * 1024; // 8K传输
		byte[] buf = new byte[bufLen];
		while ((len = in.read(buf)) != -1) {
			out.write(buf, 0, len);
		}
		out.flush();
	}
	
	public static void copyStream(InputStream in, OutputStream out, long size) throws IOException {
		int len = 0;
		int bufLen = 8 * 1024; // 8K传输
		byte[] buf = new byte[bufLen];
		long readed = 0;
		int canread = bufLen;
		while ((len = in.read(buf, 0, canread)) != -1) {
			out.write(buf, 0, len);
			readed += len;
			canread = (size - readed < (long)bufLen) ? 
					(int)(size - readed) : bufLen;  
		}
		out.flush();
	}

	
}
