package org.duomn.ichat.util;

import java.awt.GridBagConstraints;
import java.awt.Insets;

public class GBC extends GridBagConstraints {
	
	private static final long serialVersionUID = -5067285008168484512L;

	public GBC(int gridx, int gridy, int gridwidth, int gridheight) {
		this.gridx = gridx;
		this.gridy = gridy;
		this.gridwidth = gridwidth;
		this.gridheight = gridheight;
	}
	
	/**
	 * 设置位置
	 * @param anchor
	 * @return
	 */
	public GBC setAnchor(int anchor) {
		this.anchor = anchor;
		return this;
	}
	
	/**
	 * 设置放大策略
	 * @param fill [GBC.HORIZONTAL、GBC.VERTICAL、GBC.BOTH]
	 * @return
	 */
	public GBC setFill(int fill) {
		this.fill = fill;
		return this;
	}
	
	/**
	 * 设置放大的权重
	 * @param weightx
	 * @param weighty
	 * @return
	 */
	public GBC setWeight(float weightx, float weighty) {
		this.weightx = weightx;
		this.weighty = weighty;
		return this;
	}
	
	/**
	 * 设置Padding，内部填充
	 * @param ipadx
	 * @param ipady
	 * @return
	 */
	public GBC setPadding(int ipadx, int ipady) {
		this.ipadx = ipadx;
		this.ipady = ipady;
		return this;
	}
	
	/**
	 * 设置Margin，外部填充
	 * @param inset
	 * @return
	 */
	public GBC setMargin(int inset) {
		Insets insets = new Insets(inset, inset, inset, inset);
		this.insets = insets;
		return this;
	}
	
	/**
	 * 设置Margin，外部填充
	 * @param insetx 水平方向
	 * @param insety 垂直方向
	 * @return
	 */
	public GBC setMargin(int insetx, int insety) {
		Insets insets = new Insets(insetx, insety, insetx, insety);
		this.insets = insets;
		return this;
	}
	
	/**
	 * 设置Margin，外部填充
	 * @param top
	 * @param left
	 * @param bottom
	 * @param right
	 * @return
	 */
	public GBC setMargin(int top, int left, int bottom, int right) {
		Insets insets = new Insets(top, left, bottom, right);
		this.insets = insets;
		return this;
	}
}
