package org.duomn.ichat.util;

import java.util.Properties;

/**
 * 系统的环境变量
 * @author duomn
 *
 */
public class Const {
	
	public static String USER_SELF = "user.self";
	
	public static String USER_SELF_ID = "user.self.id";
	
	public static String USER_SELF_PORT = "user.self.port";
	
	public static String USER_MAP = "user.map";
	
	private static Properties props = System.getProperties();
	
	public synchronized static void put(Object key , Object value) {
		props.put(key, value);
	}
	
	@SuppressWarnings("unchecked")
	public synchronized static <T> T get(Object key) {
		return (T) props.get(key);
	}

}
