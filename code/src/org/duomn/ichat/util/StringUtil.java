package org.duomn.ichat.util;

public class StringUtil {
	
	public static String join(String[] arr, String join) { 
		if (arr == null) throw new NullPointerException("StringUtil.join()的第一个参数不允许为空");
		if (join == null) throw new NullPointerException("StringUtil.join()的第二个参数不允许为空");
		StringBuffer buf = new StringBuffer();
		for (String str : arr) {
			buf.append(str).append(join);
		}
		if (buf.length() > 0) buf.delete(buf.length() - join.length(), buf.length());
		return buf.toString();
	}

}
