package org.duomn.ichat.util;

import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public class IconUtil {
	
	private static Map<String, Icon> _map = new HashMap<String, Icon>();
	
	public static Icon getIcon(String name) {
		return getIcon(name, 0, 0);
	}
	
	public static Icon getIcon(String name, int width, int height) {
		String key = name + "#" + width + "#" + height;
		Icon icon = _map.get(key);
		if (icon == null) {
			ImageIcon ii = new ImageIcon(name);
			if (width == 0 || height == 0) {
				icon = ii;
			} else {
				Image image = ii.getImage();
				Image smallImage = image.getScaledInstance(width, height, Image.SCALE_FAST);
				icon = new ImageIcon(smallImage);
			}
			_map.put(key, icon);
		}
		return icon;
	}

}
