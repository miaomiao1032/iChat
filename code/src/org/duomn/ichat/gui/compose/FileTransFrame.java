package org.duomn.ichat.gui.compose;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import org.duomn.ichat.util.GBC;

public class FileTransFrame extends JFrame {
	private static final long serialVersionUID = 5958169021684280868L;
	
	private Border emptyBorder = BorderFactory.createEmptyBorder(1, 4, 1, 4);

	public FileTransFrame(File[] file) {
		setTitle("传送文件");
		setLocationByPlatform(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(new Dimension(200, 400));
		FileInfo[] fis = new FileInfo[4];
		for (int i = 0; i < 4; i++) {
			fis[i] = new FileInfo();
			fis[i].fileName = "文件名称" + i;
			fis[i].fullPath = "路径" + i;
			fis[i].humanSize = "4.00kb";
			fis[i].size = 4000L;
		}
		JList list = new JList(fis);
		list.setCellRenderer(new FileListRenderer());
		getContentPane().add(list);
		
		setVisible(true);
	}
	
	private class FileListRenderer extends JPanel implements ListCellRenderer {
		private static final long serialVersionUID = 8231010513818327435L;
		
		JLabel nameLbl;
		JLabel precentLbl;
		JProgressBar progress;
		JLabel sendLbl;
		JLabel cancelLbl;
 		
		public FileListRenderer() {
			setBorder(emptyBorder);
			setLayout(new GridBagLayout());
			nameLbl = new JLabel("wenjian1");
			progress = new JProgressBar();
			precentLbl = new JLabel("20%");
			sendLbl = new JLabel("另存为");
			cancelLbl = new JLabel("取消");
			add(nameLbl, new GBC(0, 0, 3, 1).setAnchor(GBC.WEST).setWeight(1.f, 0));
			add(progress, new GBC(0, 1, 3, 1).setFill(GBC.HORIZONTAL).setWeight(1.f, 0.f));
			add(precentLbl, new GBC(0, 2, 1, 1).setAnchor(GBC.WEST).setWeight(1.0f, 0));
			add(sendLbl, new GBC(1, 2, 1, 1));
			add(cancelLbl, new GBC(2, 2, 1, 1));
		}
		
		@Override
		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {
			FileInfo fi = (FileInfo) value;
			nameLbl.setText(fi.fileName + "(" + fi.humanSize + ")");
			progress.setValue(0);
			precentLbl.setText("0%");
			return this;
		}

	}
	
	private class FileInfo {
		/** 文件的全路径名称 */
		String fullPath;
		/** 文件的大小 */
		long size;
		/** 显示的名称 */
		String fileName;
		/** 便于人阅读的文件大小 */
		String humanSize;
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new FileTransFrame(null);
			}
		});
	}

}
