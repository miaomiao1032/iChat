package org.duomn.ichat.gui.custom;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;

public class SuspensionUI extends JFrame {
	private static final long serialVersionUID = 3557221457564651900L;
	
	public SuspensionUI() {
		setUndecorated(true); // 去掉标题
		setSize(300, 300);
		TransparentBackground lbl = new TransparentBackground(this);
		lbl.setPreferredSize(new Dimension(100, 100));
		lbl.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		lbl.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				System.out.println("我被点击了，决定退出");
				SuspensionUI.this.dispose();
			}
		});
		getContentPane().setLayout(new FlowLayout());;
		getContentPane().add(lbl);
		setVisible(true);
	}
	

	private class TransparentBackground extends JComponent {
		private static final long serialVersionUID = -945081104133852550L;
		
		JFrame frame;
	    private Image background;
		public TransparentBackground(JFrame frame) {
			this.frame = frame;
		    updateBackground( );
		}
		
		public void updateBackground( ) {
		    try {
		    	BufferedImage image = 
		    			new BufferedImage(frame.getWidth(), frame.getHeight(), BufferedImage.TYPE_INT_ARGB);
		    	Graphics2D g = image.createGraphics();
		    	g.setColor(new Color(255, 0, 0, 10));
		    	g.fillRect(0, 0, frame.getWidth(), frame.getHeight());
		    	
		    	g.drawRect(0, 0, frame.getWidth(), frame.getHeight());
		    	
		    	background = image;
		    } catch (Exception ex) {
		        ex.printStackTrace( );
		    }
		}
		
		public void paintComponent(Graphics g) {
		    g.drawImage(background,0,0,null);
		}
	}

	public static void main(String[] args) {
		new SuspensionUI();
	}
}
