package org.duomn.ichat.gui.custom;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.border.Border;

public class RoundBorder implements Border {
	private Color roundColor = new Color(174, 208, 196);
	private Insets insets = new Insets(0, 0, 0, 0);

	@Override
	public void paintBorder(Component c, Graphics g, int x, int y, int width,
			int height) {
		g.setColor(roundColor);
		g.drawRoundRect(x, y, c.getWidth() - 1, c.getWidth() - 1, 5, 5);
	}

	/** 设置填充 */
	public Insets getBorderInsets(Component c) {
		return insets;
	}

	/** 是否绘制 */
	public boolean isBorderOpaque() {
		return false;
	}

}
